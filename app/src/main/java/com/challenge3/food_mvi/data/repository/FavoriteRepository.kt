package com.challenge3.food_mvi.data.repository

import com.challenge3.food_mvi.data.database.FoodDao
import javax.inject.Inject

class FavoriteRepository @Inject constructor(private val dao: FoodDao) {
    fun getFavorites() = dao.getAllFoods()
}