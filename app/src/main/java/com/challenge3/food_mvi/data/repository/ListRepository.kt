package com.challenge3.food_mvi.data.repository

import com.challenge3.food_mvi.data.server.ApiServices
import javax.inject.Inject

class ListRepository @Inject constructor(private val api: ApiServices) {

    suspend fun randomFood() = api.foodRandom()
    suspend fun categoryList() = api.categoriesList()
    suspend fun foodsList(letter: String) = api.foodsList(letter)
    suspend fun searchFood(letter: String) = api.searchFood(letter)
    suspend fun foodByCategory(letter: String) = api.foodsByCategory(letter)

}