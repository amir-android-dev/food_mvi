package com.challenge3.food_mvi.data.repository

import com.challenge3.food_mvi.data.database.FoodDao
import com.challenge3.food_mvi.data.database.FoodEntity
import com.challenge3.food_mvi.data.server.ApiServices
import javax.inject.Inject

class DetailRepository @Inject constructor(private val dao: FoodDao, private val api: ApiServices) {

    suspend fun saveFavorite(entity: FoodEntity) = dao.saveFood(entity)
    suspend fun deleteFavorite(entity: FoodEntity) = dao.deleteFood(entity)
    suspend fun loadDeteil(foodId: Int) = api.foodDetail(foodId)
    fun foodExists(foodId: Int) = dao.existsFood(foodId)
}