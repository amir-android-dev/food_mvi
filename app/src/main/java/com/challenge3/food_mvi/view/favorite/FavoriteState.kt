package com.challenge3.food_mvi.view.favorite

import com.challenge3.food_mvi.data.database.FoodEntity

sealed class FavoriteState {
    object Empty : FavoriteState()
    data class GetFavorites(val list: List<FoodEntity>) : FavoriteState()
}