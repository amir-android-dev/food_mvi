package com.challenge3.food_mvi.view.detail

import com.challenge3.food_mvi.data.database.FoodEntity

sealed class DetailIntent {
    object BackToMain : DetailIntent()
    data class LoadDetail(val id: Int) : DetailIntent()
    data class ExistsAsFavorite(val id: Int) : DetailIntent()
    data class SaveFavorite(val entity: FoodEntity) : DetailIntent()
    data class DeleteFavorite(val entity: FoodEntity) : DetailIntent()

}