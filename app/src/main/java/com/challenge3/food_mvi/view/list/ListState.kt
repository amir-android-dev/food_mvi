package com.challenge3.food_mvi.view.list

import com.challenge3.food_mvi.data.model.ResponseCategoriesList
import com.challenge3.food_mvi.data.model.ResponseFoodsList

sealed class ListState {
    object Idle : ListState()
    object LoadingCategory : ListState()
    object LoadingFoods : ListState()
    object Empty:ListState()
    data class FilterLetters(val letters: MutableList<Char>) : ListState()
    data class RandomFood(val food: ResponseFoodsList.Meal?) : ListState()
    data class CategoriesList(val categoryList: List<ResponseCategoriesList.Category>) : ListState()
    data class FoodsList(val foods: List<ResponseFoodsList.Meal>) : ListState()

    data class Error(val error: String) : ListState()

}
