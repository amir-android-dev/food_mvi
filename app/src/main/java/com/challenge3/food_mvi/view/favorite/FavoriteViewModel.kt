package com.challenge3.food_mvi.view.favorite

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.challenge3.food_mvi.data.repository.FavoriteRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoriteViewModel @Inject constructor(private val repo: FavoriteRepository) : ViewModel() {
    val intentChannel = Channel<FavoriteIntent>()

    init {
        handleIntent()
    }

    private val _state = MutableStateFlow<FavoriteState>(FavoriteState.Empty)
    val state get(): StateFlow<FavoriteState> = _state

    private fun handleIntent() = viewModelScope.launch {
        intentChannel.consumeAsFlow().collect { intent ->
            when (intent) {
                FavoriteIntent.GetFavorites -> fetchFavoriteFoods()
            }
        }
    }

    private fun fetchFavoriteFoods() = viewModelScope.launch {
        repo.getFavorites().collect { foods ->
            if (foods.isEmpty()) {
                _state.value = FavoriteState.Empty
            } else {
                _state.value = FavoriteState.GetFavorites(foods)
            }

        }

    }


}