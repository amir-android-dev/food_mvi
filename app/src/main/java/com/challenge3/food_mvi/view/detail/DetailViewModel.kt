package com.challenge3.food_mvi.view.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.challenge3.food_mvi.data.database.FoodEntity
import com.challenge3.food_mvi.data.repository.DetailRepository
import com.challenge3.food_mvi.view.list.ListState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(private val repo: DetailRepository) : ViewModel() {

    val intentChannel = Channel<DetailIntent>()

    private val _state = MutableStateFlow<DetailState>(DetailState.Loading)
    val state: StateFlow<DetailState> get() = _state

    init {
        handleIntent()
    }

    private fun handleIntent() = viewModelScope.launch {
        intentChannel.consumeAsFlow().collect { intent ->
            when (intent) {
                DetailIntent.BackToMain -> backToMainPage()
                is DetailIntent.DeleteFavorite -> deleteFavorite(intent.entity)
                is DetailIntent.ExistsAsFavorite -> favoriteExists(intent.id)
                is DetailIntent.LoadDetail -> loadDetail(intent.id)
                is DetailIntent.SaveFavorite -> saveFavorite(intent.entity)
            }
        }
    }

    private fun backToMainPage() = viewModelScope.launch {
        _state.emit(DetailState.BackToMain)
    }

    private fun loadDetail(id: Int) = viewModelScope.launch {
        val response = repo.loadDeteil(id)
        _state.emit(DetailState.Loading)
        when (response.code()) {
            in 200..202 -> {
                _state.emit(DetailState.LoadFood(response.body()!!))
            }
            in 400..499 -> {
                _state.emit(DetailState.Error(""))
            }
            in 500..599 -> {
                _state.emit(DetailState.Error(""))
            }
        }
    }

    private fun deleteFavorite(entity: FoodEntity) = viewModelScope.launch {
        _state.emit(DetailState.DeleteFood(repo.deleteFavorite(entity)))
    }

    private fun saveFavorite(entity: FoodEntity) = viewModelScope.launch {
        _state.emit(DetailState.DeleteFood(repo.saveFavorite(entity)))
    }

    private fun favoriteExists(id: Int) = viewModelScope.launch {
        repo.foodExists(id).collect {
            _state.value = DetailState.FoodExisted(it)
        }
    }
}