package com.challenge3.food_mvi.view.list

sealed class ListIntent {
    object LoadFilterLetters : ListIntent()
    object RandomFood : ListIntent()
    object LoadCategoryList : ListIntent()
    data class LoadFoodsList(val letter: String) : ListIntent()
    data class LoadSearchFoods(val search: String) : ListIntent()
    data class LoadFoodsByCategory(val category: String) : ListIntent()

}
