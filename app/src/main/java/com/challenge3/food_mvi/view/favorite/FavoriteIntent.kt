package com.challenge3.food_mvi.view.favorite

sealed class FavoriteIntent {
    object GetFavorites : FavoriteIntent()
}
