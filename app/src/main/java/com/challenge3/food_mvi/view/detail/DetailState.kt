package com.challenge3.food_mvi.view.detail

import com.challenge3.food_mvi.data.model.ResponseFoodsList

sealed class DetailState {
    object BackToMain : DetailState()
    object Loading : DetailState()
    data class LoadFood(val data: ResponseFoodsList) : DetailState()
    data class Error(val msg: String) : DetailState()
    data class SaveFood(val unit: Unit) : DetailState()
    data class DeleteFood(val unit: Unit) : DetailState()
    data class FoodExisted(val exists: Boolean) : DetailState()

}