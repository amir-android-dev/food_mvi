package com.challenge3.food_mvi.utils.network

import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Build
import com.challenge3.food_mvi.utils.network.ConnectivityStatus.*
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch
import javax.inject.Inject

class NetworkConnectivity @Inject constructor(
    private val cm: ConnectivityManager,
    private val request: NetworkRequest
) : ConnectivityStatus {


    override fun observe(): Flow<Status> {
        return callbackFlow {

            val callback = object : ConnectivityManager.NetworkCallback() {
                override fun onAvailable(network: Network) {
                    super.onAvailable(network)
                    launch { send(Status.Available) }
                }

                override fun onUnavailable() {
                    super.onUnavailable()
                    launch { send(Status.Unavailable) }
                }

                override fun onLosing(network: Network, maxMsToLive: Int) {
                    super.onLosing(network, maxMsToLive)
                    launch { send(Status.Losing) }
                }

                override fun onLost(network: Network) {
                    super.onLost(network)
                    launch { send(Status.Lost) }
                }
            }
            //register
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                //more than 24
                cm.registerDefaultNetworkCallback(callback)
            } else {
                //less than 24
                cm.registerNetworkCallback(request, callback)
            }
            //unregister
            awaitClose {
                cm.unregisterNetworkCallback(callback)
            }
        }.distinctUntilChanged()

    }
}