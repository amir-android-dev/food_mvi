package com.challenge3.food_mvi.ui.detail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import coil.load
import com.challenge3.food_mvi.data.database.FoodEntity
import com.challenge3.food_mvi.ui.detail.player.PlayerActivity
import com.challenge3.food_mvi.utils.*
import com.challenge3.food_mvi.utils.network.ConnectivityStatus
import com.challenge3.food_mvi.utils.network.NetworkConnectivity
import com.challenge3.food_mvi.view.detail.DetailIntent
import com.challenge3.food_mvi.view.detail.DetailState
import com.challenge3.food_mvi.view.detail.DetailViewModel
import com.ema.food_mvi.R
import com.ema.food_mvi.databinding.FragmentFoodDetailBinding
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import org.json.JSONObject
import javax.inject.Inject

@AndroidEntryPoint
class FoodDetailFragment : Fragment() {
    private lateinit var binding: FragmentFoodDetailBinding

    //other
    private val args: FoodDetailFragmentArgs by navArgs()
    private var foodId = 0

    @Inject
    lateinit var entity: FoodEntity

    @Inject
    lateinit var networkConnectivity: NetworkConnectivity

    private var isFavorite = false
    private val viewModel: DetailViewModel by viewModels()

    enum class PageState { EMPTY, NETWORK, NONE }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentFoodDetailBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            //back
            detailBack.setOnClickListener {
                lifecycleScope.launch {
                    viewModel.intentChannel.send(
                        DetailIntent.BackToMain
                    )
                }
            }
            lifecycleScope.launch {
                viewModel.state.collect { state ->
                    when (state) {
                        is DetailState.BackToMain -> findNavController().navigateUp()
                        is DetailState.Loading -> detailLoading.isVisible(true, detailContentLay)
                        is DetailState.LoadFood -> {
                            detailLoading.isVisible(false, detailContentLay)
                            //Set data
                            state.data.meals?.get(0)?.let { mealDetail ->
                                //Entity
                                entity.id = mealDetail.idMeal!!.toInt()
                                entity.title = mealDetail.strMeal.toString()
                                entity.img = mealDetail.strMealThumb.toString()
                                //Update UI
                                foodCoverImg.load(mealDetail.strMealThumb) {
                                    crossfade(true)
                                    crossfade(500)
                                }
                                foodCategoryTxt.text = mealDetail.strCategory
                                foodAreaTxt.text = mealDetail.strArea
                                foodTitleTxt.text = mealDetail.strMeal
                                foodDescTxt.text = mealDetail.strInstructions
                                //Play
                                if (mealDetail.strYoutube != null) {
                                    foodPlayImg.visibility = View.VISIBLE
                                    foodPlayImg.setOnClickListener {
                                        val videoId = mealDetail.strYoutube.split("=")[1]
                                        Intent(requireContext(), PlayerActivity::class.java).also {
                                            it.putExtra(VIDEO_ID, videoId)
                                            startActivity(it)
                                        }
                                    }
                                } else {
                                    foodPlayImg.visibility = View.GONE
                                }
                                //Source
                                if (mealDetail.strSource != null) {
                                    foodSourceImg.visibility = View.VISIBLE
                                    foodSourceImg.setOnClickListener {
                                        startActivity(
                                            Intent(
                                                Intent.ACTION_VIEW,
                                                Uri.parse(mealDetail.strSource)
                                            )
                                        )
                                    }
                                } else {
                                    foodSourceImg.visibility = View.GONE
                                }
                                //Json Array
                                val jsonData = JSONObject(Gson().toJson(state.data))
                                val meals = jsonData.getJSONArray("meals")
                                val meal = meals.getJSONObject(0)
                                //Ingredient
                                for (i in 1..15) {
                                    val ingredient = meal.getString("strIngredient$i")
                                    if (ingredient.isNullOrEmpty().not()) {
                                        ingredientsTxt.append("$ingredient\n")
                                    }
                                }
                                //Measure
                                for (i in 1..15) {
                                    val measure = meal.getString("strMeasure$i")
                                    if (measure.isNullOrEmpty().not()) {
                                        measureTxt.append("$measure\n")
                                    }
                                }
                            }
                        }

                        is DetailState.DeleteFood -> {}
                        is DetailState.Error -> {
                            detailLoading.isVisible(false, detailContentLay)
                            displayToast(requireContext(), state.msg)
                        }

                        is DetailState.FoodExisted -> {
                            isFavorite = state.exists
                            if (state.exists) {
                                detailFav.setColorFilter(
                                    ContextCompat.getColor(
                                        requireContext(),
                                        R.color.tartOrange
                                    )
                                )
                            } else {
                                detailFav.setColorFilter(
                                    ContextCompat.getColor(
                                        requireContext(),
                                        R.color.black
                                    )
                                )
                            }
                        }

                        is DetailState.SaveFood -> {}
                    }
                }
            }
            //save & delete
            detailFav.setOnClickListener {
                lifecycleScope.launch {
                    if (isFavorite) {
                        viewModel.intentChannel.send(DetailIntent.DeleteFavorite(entity))
                    } else {
                        viewModel.intentChannel.send(DetailIntent.SaveFavorite(entity))
                    }
                }
            }
            //connection
            lifecycleScope.launch {
                networkConnectivity.observe().collect {
                    when (it) {
                        ConnectivityStatus.Status.Available -> {
                            checkConnectionOrEmpty(false, PageState.NONE)
                            //send on channel
                            lifecycleScope.launch {
                                viewModel.intentChannel.send(DetailIntent.LoadDetail(args.foodId))
                                viewModel.intentChannel.send(DetailIntent.ExistsAsFavorite(args.foodId))
                            }

                        }

                        ConnectivityStatus.Status.Unavailable -> {}
                        ConnectivityStatus.Status.Losing -> {}
                        ConnectivityStatus.Status.Lost ->{
                            checkConnectionOrEmpty(true, PageState.NETWORK)
                        }
                    }
                }
            }

        }
    }

    //Internet
    private fun checkConnectionOrEmpty(isShownError: Boolean, state: PageState) {
        binding.apply {
            if (isShownError) {
                homeDisLay.isVisible(true, detailContentLay)
                when (state) {
                    PageState.EMPTY -> {
                        statusLay.disImg.setImageResource(R.drawable.box)
                        statusLay.disTxt.text = getString(R.string.emptyList)
                    }

                    PageState.NETWORK -> {
                        statusLay.disImg.setImageResource(R.drawable.disconnect)
                        statusLay.disTxt.text = getString(R.string.checkInternet)
                    }

                    else -> {}
                }
            } else {
                homeDisLay.isVisible(false, detailContentLay)
            }
        }
    }
}
