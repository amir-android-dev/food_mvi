package com.challenge3.food_mvi.ui.list

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import com.challenge3.food_mvi.ui.list.adapters.CategoriesAdapter
import com.challenge3.food_mvi.ui.list.adapters.FoodsAdapter
import com.challenge3.food_mvi.utils.*
import com.challenge3.food_mvi.utils.network.ConnectivityStatus
import com.challenge3.food_mvi.utils.network.NetworkConnectivity
import com.challenge3.food_mvi.view.list.FoodsListViewModel
import com.challenge3.food_mvi.view.list.ListIntent
import com.challenge3.food_mvi.view.list.ListState
import com.ema.food_mvi.R
import com.ema.food_mvi.databinding.FragmentFoodsListBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class FoodsListFragment : Fragment() {
    //Binding
    private var _binding: FragmentFoodsListBinding? = null
    private val binding get() = _binding

    @Inject
    lateinit var categoriesAdapter: CategoriesAdapter

    @Inject
    lateinit var foodsAdapter: FoodsAdapter

    @Inject
    lateinit var networkConnectivity: NetworkConnectivity

    //Other
    private val viewModel: FoodsListViewModel by viewModels()

    enum class PageState { EMPTY, NETWORK, NONE }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentFoodsListBinding.inflate(layoutInflater)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //InitViews
        binding?.apply {
            //Lifecycle
            lifecycleScope.launchWhenCreated {
                //Get data
                viewModel.state.collect { state ->
                    when (state) {
                        is ListState.Idle -> {}
                        is ListState.FilterLetters -> {
                            filterSpinner.setupListWithAdapter(state.letters) {
                                lifecycleScope.launch {
                                    viewModel.intentChannel.send(ListIntent.LoadFoodsList(it))
                                }
                            }
                        }
                        is ListState.RandomFood -> {
                            if (state.food != null) {
                                headerImg.load(state.food.strMealThumb) {
                                    crossfade(true)
                                    crossfade(500)
                                }
                            }
                        }
                        is ListState.LoadingCategory -> {
                            homeCategoryLoading.isVisible(true, categoryList)
                        }
                        is ListState.CategoriesList -> {
                            homeCategoryLoading.isVisible(false, categoryList)
                            categoriesAdapter.setData(state.categoryList)
                            categoryList.setupRecyclerView(
                                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false),
                                categoriesAdapter
                            )

                            categoriesAdapter.setOnItemClickListener {
                                lifecycleScope.launchWhenCreated {
                                    viewModel.intentChannel.send(ListIntent.LoadFoodsByCategory(it.strCategory!!))
                                }
                            }
                        }
                        is ListState.LoadingFoods -> {
                            homeFoodsLoading.isVisible(true, foodsList)
                        }
                        is ListState.FoodsList -> {
                            checkConnectionOrEmpty(false, PageState.NONE)
                            homeFoodsLoading.isVisible(false, foodsList)
                            foodsAdapter.setData(state.foods)
                            foodsList.setupRecyclerView(
                                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false),
                                foodsAdapter
                            )

                            foodsAdapter.setOnItemClickListener {
                                val direction = FoodsListFragmentDirections.actionFoodsListFragmentToFoodDetailFragment(it.idMeal?.toInt()!!)
                                findNavController().navigate(direction)
                            }
                        }
                        is ListState.Empty -> {
                            checkConnectionOrEmpty(true, PageState.EMPTY)
                        }
                        is ListState.Error -> {
                            displayToast(requireContext(),state.error)
                        }
                    }
                }
            }
            //Search
            searchEdt.addTextChangedListener {
                if (it.toString().length > 2) {
                    lifecycleScope.launch {
                        viewModel.intentChannel.send(ListIntent.LoadSearchFoods(it.toString()))
                    }
                }
            }
            //Check internet
            lifecycleScope.launchWhenCreated {
                networkConnectivity.observe().collect {
                    when (it) {
                        ConnectivityStatus.Status.Available -> {
                            checkConnectionOrEmpty(false, PageState.NONE)
                            viewModel.intentChannel.send(ListIntent.LoadFilterLetters)
                            viewModel.intentChannel.send(ListIntent.RandomFood)
                            viewModel.intentChannel.send(ListIntent.LoadCategoryList)
                            viewModel.intentChannel.send(ListIntent.LoadFoodsList("A"))
                        }
                        ConnectivityStatus.Status.Unavailable -> {}
                        ConnectivityStatus.Status.Losing -> {}
                        ConnectivityStatus.Status.Lost -> {
                            checkConnectionOrEmpty(true, PageState.NETWORK)
                        }
                    }
                }
            }
        }
    }

    private fun checkConnectionOrEmpty(isShownError: Boolean, state: PageState) {
        binding?.apply {
            if (isShownError) {
                homeDisLay.isVisible(true, homeContent)
                when (state) {
                    PageState.EMPTY -> {
                        statusLay2.disImg.setImageResource(R.drawable.box)
                        statusLay2.disTxt.text = getString(R.string.emptyList)
                    }
                    PageState.NETWORK -> {
                        statusLay2.disImg.setImageResource(R.drawable.disconnect)
                        statusLay2.disTxt.text = getString(R.string.checkInternet)
                    }
                    else -> {}
                }
            } else {
                homeDisLay.isVisible(false, homeContent)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
//an issue