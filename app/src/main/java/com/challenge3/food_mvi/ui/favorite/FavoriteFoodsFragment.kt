package com.challenge3.food_mvi.ui.favorite

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.challenge3.food_mvi.utils.isVisible
import com.challenge3.food_mvi.utils.setupRecyclerView
import com.challenge3.food_mvi.view.favorite.FavoriteIntent
import com.challenge3.food_mvi.view.favorite.FavoriteState
import com.challenge3.food_mvi.view.favorite.FavoriteViewModel
import com.ema.food_mvi.R

import com.ema.food_mvi.databinding.FragmentFavoriteFoodsBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class FavoriteFoodsFragment : Fragment() {
    //binding
    private lateinit var binding: FragmentFavoriteFoodsBinding

    //other
    private val viewModel: FavoriteViewModel by viewModels()

    @Inject
    lateinit var adapter: FavoriteAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentFavoriteFoodsBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            lifecycleScope.launch {
                viewModel.intentChannel.send(FavoriteIntent.GetFavorites)
            }
            binding.apply {
                lifecycleScope.launch {
                    viewModel.state.collect {
                        when (it) {
                            is FavoriteState.GetFavorites -> {
                                emptyLay.isVisible(false, favoriteList)
                                favoriteList.setupRecyclerView(
                                    LinearLayoutManager(requireContext()),
                                    adapter
                                )
                                adapter.setData(it.list)
                                adapter.setOnItemClickListener {food->
                                val action = FavoriteFoodsFragmentDirections.actionFoodsListFragmentToFoodDetailFragment(food.id)
                                    Navigation.findNavController(requireView()).navigate(action)

                                }
                            }
                            FavoriteState.Empty -> {
                                emptyLay.isVisible(true, favoriteList)
                                statusLay.disImg.setImageResource(R.drawable.box)
                                statusLay.disTxt.text = getString(R.string.list_is_empty)
                            }


                        }
                    }
                }

            }


        }
    }

}